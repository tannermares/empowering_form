$(document).ready(function() {
  function buildQuestion(subcategory, question, number)
	{
		$('#'+subcategory).append(
		'<div class="span10">'+
    	'<span class="span9">' + question + '</span>'+
      '<select class="span1" id="' + number + '">'+
				'<option value="1">1</option>'+
				'<option value="2">2</option>'+
				'<option value="3">3</option>'+
				'<option value="4">4</option>'+
				'<option value="5">5</option>'+
      '</select>'+
	  '</div>');
	}
	function buildSubcat(category, subcategory)
	{
		var subcategoryName = subcategory[0].replace(/[^\w]/g,'')
		$('#'+category).append('<div id="' + subcategoryName + '" class="span11"><h5>' + subcategory[0] + '</h5></div>');
		for(var i = 1; i < subcategory.length; i++)
		{
			for(var j = 0; j < subcategory[i].length; j++)
			{
				buildQuestion(subcategoryName, subcategory[i][j], i+j);	
			}
		}
	}
	function buildCategory(category)
	{
		var categoryName = category[0].replace(/[^\w]/g,'')
		$('#questions').append('<div id="' + categoryName + '"></div>');
		$('#' + categoryName).append('<legend>' + category[0] + '</legend>');
		for(var i = 1; i < category.length; i++)
		{
			buildSubcat(categoryName, category[i]);
		}
	}
	function buildQuestions(questions)
	{
		for(var i = 0; i < questions.length; i++)
		{
			buildCategory(questions[i]);
		}
	}
	var questionsArray = 
	[
		['Leadership', 
			['Professional Responsibilities/DC Code of Responsibility', 
				[
					'I take ownership over my success, no matter what my employment status.',
					'I acknowledge that I am a professional, and as such will engage in consistent activity to enhance my professional image.',
					'I am involved in my community.',
					'I continue to learn by reading peer reviewed journals weekly.',
					'I keep current on research related to my profession.',
					'I continue to refine my skills as a clinician via yearly seminars.',
					'I belong to my local, state, and national associations.',
					'I network/meet with fellow DCs on a regular basis.',
					'I network with other medical professionals on a regular basis.',
					'I routinely brainstorm and develop marketing ideas.',
					'I routinely examine and improve upon current business systems.',
					'I review my statistics on a weekly, monthly, and yearly basis.',
					'I treat staff and patients as I would want to be treated.',
					'I am proactive, not reactive.',
					'I live by this guiding principle: "Give a patient what they need, only what they need, nothing more and nothing less, always."'
				]
			],
			['Time Management', 
				[
					'I set aside time to work "on" versus "in" my practice.',
					'I use "controlling" calendars to monitor each staff position and special projects.',
					'I’ve identified time bandits regarding my personal time.',
					'I’ve identified time bandits related to each staff position.'
				]
			],
			['Research',
				[
					'Rate your efforts on becoming an "evidence-informed" office.',
					'Rate your familiarity with CCGPP and articles and research papers on its website.',
					'Rate your efforts to read JMPT and/or other industry-related peer reviewed journals.',
					'Rate how current you are regarding the CCGPP acute and chronic guidelines.',
					'Rate how familiar you are with the CCGPP paper published regarding “terminology”.'
				]
			]
		],
		['Business Organization/Operation',
			['Employee Training', 
				[
					'Do you meet with your staff to review and establish systems?',
					'Do you regularly meet with your staff to conduct ongoing training?',
					'Do you have an employee manual/handbook?',
					'Have you established a system for staff to report issues/problem?',
					'Are all systems per department in writing for ease of training?',
					'Are your employees cross-trained?'
				]
			],
			['Employee Payroll', 
				[
					'Did you establish an income cap per position for your company?',
					'Did you review the income cap with each employee?',
					'Do you periodically review performance and benefits with each employee?',
					'Do you have an accurate system for each employee to report time worked and/or changes to payroll information?',
					'Rate the satisfaction with your payroll processing service (even if it is in-house).'
				]
			],
			['Administration',
				[
					'I have developed an employee handbook and provided it to each staff person.',
					'Rate the consistency of meeting with my individual staff members regularly.',
					'Rate quality of your office scripts.',
					'Rate the quality of your ongoing training programs for staff.',
					'Rate the quality of your hiring/firing systems.'
				]
			]
		],
		['Money/Budget',
			['Cash Handling',
				[
					'Do your documents clearly specify initial visit services?',
					'Does more than one person handle cash processing, preparing the deposit/balance?',
					'Do you make daily deposits?',
					'Do you rotate the employees who handle money?'
				]
			],
			['Credit', 
				[
					'Do you control the amount of credit extended to patients?',
					'Do you use financial programs (ex. Care Credit) to extend credit?',
					'Have you limited the amount of charity care provided each month?'
				]
			],
			['Financial = Basic Business Principles',
				[
					'Is your business profitable AFTER you pay yourself?',
					'Have you developed a business plan 1-3-5-10 years?',
					'Have you grown personally and learned to say “NO”?',
					'Have you established a budget?',
					'Do you review each line item of your budget monthly?',
					'Is 10% of your budget targeted to marketing?',
					'Do you stick to your budget?',
					'Have you lowered your stress by “systemizing” your business?',
					'Have you considered your history, what worked, what didn’t?',
					'Do you have a written “strategic objective”?',
					'Have you formed a corporation and hired yourself as an employee?',
					'Does your payroll system pay weekly?',
					'Do you pay bills weekly?',
					'Do you have an expense pre-approval system?',
					'Have you established a “wealth” and an “emergency” account?',
					'Do you pay yourself and then your wealth account before paying bills?',
					'Have you obtained a line of credit from your bank?',
					'Do you meet with your accountant regularly (monthly preferable)?',
					'Do you hold a weekly “financial” department meeting?',
					'Do you hold a weekly administrative meeting?',
					'Do you set aside time weekly for financial planning?'
				]
			],
			['Financial = Overview', 
				[
					'Do you receive/review financial reports and stats from each department weekly and/or monthly?',
					'Do you review stats on production,collections,new patients,patient visits and expenses monthly?',
					'Do you review a monthly profit/loss statement?',
					'Do you set and monitor financial goals regularly?',
					'Do you regularly review fees and adjust when appropriate?',
					'Do you audit your insurance department quarterly?',
					'Do you lock up the signature stamp every night?',
					'Do you have only one staff person in charge of signature stamp?',
					'Do you use a business checking account to pay all business expenses?',
					'Do you have an emergency account funded regularly?',
					'Do you have a wealth account funded regularly?',
					'Are your business/financial systems documented in writing?',
					'Do you understand the difference between a profit/loss statement and a balance sheet?',
					'Do you balance your business accounts every month?',
					'Do you meet with staff on a monthly basis at minimum to discuss problem accounts and issues?',
					'Do you have an efficient accounts receivable system?',
					'Do you have an efficient accounts payable system?',
					'Do you review accounts before cases are referred for collection?',
					'Do you see and sign ALL checks before paying bills?'
				]
			],
			['Primary/Secondary Billing', 
				[
					'Are the invoices easy for patients to understand?',
					'Is billing performed consistently and efficiently?',
					'Does your insurance staff produce aging reports monthly?',
					'Do you review aging reports monthly?',
					'Do you have a defined system for primary billing?',
					'Do you have a defined system for secondary billing?',
					'Do you have a defined system for sending a patient to collections?',
					'Do you send no more than 2 bills, max. 90 days, before implementing collections?',
					'Do you meet with your financial department weekly/monthly?'
				]
			],
			['Inventory Control', 
				[
					'Do you use a purchase order or similar form for purchases?',
					'Have you established pre-authorization amount for purchases?',
					'Do you require written justification for proposed non-routine purchases?',
					'Do you require a 2 CA process for requesting a purchase vs. ordering the products?',
					'Do you negotiate when possible?',
					'Do you monitor sales and reduce/eliminate "dead" products?',
					'Do you review petty cash receipts regularly?'
				]
			],
			['Accounts Payable', 
				[
					'Are all bills/invoices immediately sent to accounting system/desk?',
					'Do you verify all invoices before paid?',
					'Do you produce A/P aging reports and use them to plan payments?',
					'Do you pay all bills on time to avoid late charges and fee?',
					'Do you communicate to all vendors if bills will be paid late?',
					'Do you pay all taxes and bank loans on time?'
				]
			]
		],
		['Marketing', 
			['Marketing Summary', 
				[
					'I have taken the time to draft a paragraph..."My ideal practice."',
					'I have a written primary aim regarding my life.',
					'I have a written company story.',
					'I have a written mission and vision statement.',
					'I have a written strategic objective.',
					'I have clearly decided which types of patients I desire.',
					'I have made attempts to “brand” my office.',
					'I have considered the patient’s point of view regarding the “functionality” of my practice (what the pt gets).',
					'I have evaluated my fees on a regular basis in comparison to my competition.',
					'Patients have easy access to my office.',
					'I have developed a “unique selling proposition” for my office.',
					'I have developed a USP for each financial class in my office.',
					'Rate the sensory impact of the inside and outside of your office.'
				]
			]
		],
		['Patient Generation',
			['Inter-Professional Communication', 
				[
					'Once you request a referral, reports are sent to specialist in advance of the patient’s visit.',
					'Initial reports are routinely sent to patient’s referring medical professional or PCP (even if they did not refer the patient).',
					'Update reports are routinely sent to the patient’s referring medical professional.',
					'Discharge summaries are routinely sent to the patient’s referring medical professional.'
				]
			],
			['Patient Generating Systems', 
				[
					'Rate the quality and consistency of internal low cost programs.',
					'Rate the quality and consistency of internal high cost programs.',
					'Rate the quality and consistency of external low cost programs.',
					'Rate the quality and consistency of external high cost programs.',
					'I set aside 10% of monthly collections for marketing.',
					'I develop a yearly marketing plan and monitor it regularly.',
					'I call all new patients and referral sources after the first visit.',
					'I teach a spinal care/patient education class at least 1x/month.',
					'Rate the quality of your website.'
				]
			]
		],
		['Patient Conversion',
			['1st Visit New Patient', 
				[
					'Do you always review the chart prior to the initial consultation?',
					'Do you always greet the patient with a warm smile and handshake?',
					'Do you always inquire as to how the patient discovered the office?',
					'Do you always speak positively of the referral source?',
					'Do you explain what you are going to do, before you do it?',
					'Rate the thoroughness of your consultation/history taking.',
					'Rate the thoroughness of your examination?',
					'Do you explain each test as you perform them?',
					'Rate the clarity of your first day discharge instructions?',
					'Do you have an efficient system to transfer the patient back to the front desk?'
				]
			],
			['2nd Visit Report of Findings', 
				[
					'Rate the thoroughness of your report of findings.',
					'Do you thoroughly review each examination finding during the ROF?',
					'Did you allow the patient to ask questions?',
					'Rate the patient’s understanding of their problem and tx plan.',
					'Do you thoroughly describe the manipulation before treating?',
					'Do you provide and explain a thorough informed consent?'
				]
			]
		],
		['Patient Fullfillment/Services',
			['Case Management',
				[
					'Rate your understanding of “patient centered” vs. “doctor centered?',
					'Treatment plans are always unique to the patient.',
					'All diagnostic test recommendations are patient specific.',
					'Risk factors are considered and documented thoroughly.',
					'Barriers to recovery are identified and documented thoroughly.',
					'Second opinions considered and documented thoroughly.',
					'You work towards patient self-care/reliance.',
					'You provide exercises when appropriate.',
					'You discuss co-morbidities with your patient and provide advice when appropriate.',
					'You develop goals of care and monitor those goals.',
					'You reduce use of passive modalities and incorporate active care recommendations as the patient progresses.',
					'Once maximal therapeutic benefit is reached you recommend a therapeutic withdrawal from care.',
					'If a patient returns for care do you clinically update the chart and discuss what transpired during the gap of care.',
					'You base care on the severity of the condition vs. routine pre-determined care plans.'
				]
			],
			['Work Comp', 
				[
					'You communicate consistently with the patient’s employer or HR rep.',
					'You communicate consistently with the patient’s attorney.',
					'You understand the laws in your state governing WC.',
					'You keep rebuttals to IME denials factual vs. emotional or personal.',
					'The majority of WC pts are treated towards resolution and discharged.'
				]
			],
			['Personal Injury', 
				[
					'You communicate consistently with the patient’s attorney.',
					'You understand the laws in your state governing PI.',
					'You keep rebuttals to IME denials factual vs. emotional or personal.',
					'The majority of PI pts are treated towards resolution and discharged.',
					'If a case is beginning to appear out-of-control or simply overly expensive, do you call the attorney to discuss the case.',
					'Rate your familiarity with Colossus and/or other intelligent claims management software programs.',
					'Do you document the main cost drivers of stress, sleeping difficulties, duties under duress, etc?'
				]
			],
			['Medicare', 
				[
					'You have visited CMS website and read the chiropractic-related info.',
					'You clearly understand the CMS definitions of maintenance, acute, chronic, supportive care, etc.',
					'You clearly understand the documentation requirements of CMS.',
					'You use the proper modifiers AT and GA when necessary.',
					'You have the patient sign the ABN form when you believe care will not be paid by Medicare.',
					'Do you have a system in place to transition the patient to cash after MMI has been attained?'
				]
			],
			['Documentation', 
				[
					'Diagnostic tests are efficiently evaluated and documented thoroughly.',
					'Contraindications to treatment are clearly marked on the tx card.',
					'Idiosyncrasies are documented clearly on the tx card.',
					'Examinations are performed regularly to monitor response to care.',
					'Visual analog scores (VAS) and pain charts are completed by pt. every 6-12 visits minimally.',
					'Outcome assessment tools are utilized regularly (Roland Morris, etc.)',
					'Daily notes are specific to the patient and not redundant.'
				]
			],
			['Coding', 
				[
					'You annually review nationally recognized coding manuals to check for updates on codes.',
					'You understand the elements that support the levels of E/M codes and other codes.',
					'Are the services coded properly at each visit?',
					'Are the codes communicated/entered in the computer accurately?',
					'Do you periodically audit to insure accurate data entry?',
					'Do you use the proper modifiers when necessary?'
				]
			],
			['Managed Care', 
				[
					'Do you know what it costs you to provide care on a daily basis and have you evaluated your cost basis against the amount reimbursed by the managed care organization?',
					'Do you offer a clear transition to cash when the patient runs out of benefit?',
					'Do you know how to write an effective appeal letter when services are denied?',
					'Do you offer other cash services (orthotics, laser treatment, decompression, nutrition, etc.) in an effort o provide quality care and offset the low fees paid by MCOs?'
				]
			]
		]
	];
	buildQuestions(questionsArray)
	$('li.next').click(function() {
		var steps = questionsArray.length
		$('div.bar').width("30%");
	});
	$('li.previous').click(function() {
		$('div.bar').width("10%")
	});
});
